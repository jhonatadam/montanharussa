#include <GL/glut.h>
#include <GL/gl.h>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

#include "bib/CameraDistante.h"
#include "bib/Desenha.h"
#include "bib/Curva.h"

//variaveis globais
int width = 800;
int height = 600;

int slices = 16;
int stacks = 16;

float trans_obj = false;

float tx = 0.0;
float ty = 0.0;
float tz = 0.0;

float ax = 0.0;
float ay = 0.0;
float az = 0.0;

float delta = 5.0;

float sx = 1.0;
float sy = 1.0;
float sz = 1.0;

//ponto em coords locais, a ser calculado em coords de mundo
float pl[4] = { 0.0, 0.0, 0.0, 1.0 };
//ponto em coords globais, resultado da conversao a partir de pl
float pg[4] = { 0.0, 0.0, 0.0, 1.0 };

bool lbpressed = false;
bool mbpressed = false;
bool rbpressed = false;

float last_x = 0.0;
float last_y = 0.0;

Camera* cam = new CameraDistante();
Camera* cam2 = new CameraDistante(-3,2,-5, 0,0,0, 0,1,0);
float savedCamera[9];
bool manual_cam = false;
bool change_manual_cam = false;

static int t = 0;
static int view = 0;
static const int numViews = 3;

static bool showPoints = false;

static bool play = false;

static TipoCurva curvaSelec = B_SPLINE;

static vector<Vetor3D> pontosControle {{0,2,2}
                                      ,{-2,5,4}
                                      ,{5,2,5}
                                      ,{-1,2,3}
                                      ,{-4,5,-7}
                                      ,{6,0,-2}
                                      ,{4,7,0}
                                      ,{0,2,0}
                                      ,{0,2,2}
                                      ,{-2,5,4}
                                      ,{5,2,5}};

Curva curva(B_SPLINE);
/*
void gerar_pontos_curva()
{
    GLdouble x, y, z;
    for(int i = 1; i < int(pontos.size())-2; i++)
    {
        for(int k = 0;  k < 200; k++){ //50 pontos
           float t = k*0.005;

           x = pontos[i].x + 0.5*t*(-pontos[i-1].x+pontos[i+1].x)
               + t*t*(pontos[i-1].x - 2.5*pontos[i].x + 2*pontos[i+1].x - 0.5*pontos[i+2].x)
               + t*t*t*(-0.5 * pontos[i-1].x + 1.5 * pontos[i].x - 1.5 * pontos[i+1].x + 0.5 * pontos[i+2].x);
           y = pontos[i].y + 0.5*t*(-pontos[i-1].y + pontos[i+1].y)
               + t*t*(pontos[i-1].y - 2.5*pontos[i].y + 2*pontos[i+1].y - 0.5*pontos[i+2].y)
               + t*t*t*(-0.5*pontos[i-1].y + 1.5*pontos[i].y - 1.5*pontos[i+1].y + 0.5*pontos[i+2].y);
           z = pontos[i].z + 0.5*t*(-pontos[i-1].z + pontos[i+1].z)
               + t*t*(pontos[i-1].z - 2.5*pontos[i].z + 2*pontos[i+1].z - 0.5*pontos[i+2].z)
               + t*t*t*(-0.5*pontos[i-1].z + 1.5*pontos[i].z - 1.5*pontos[i+1].z + 0.5*pontos[i+2].z);
           pontos_curva.push_back(Vetor3D(x,y,z));

           x = 0.5*(-pontos[i-1].x+pontos[i+1].x)
               + 2*t*(pontos[i-1].x - 2.5*pontos[i].x + 2*pontos[i+1].x - 0.5*pontos[i+2].x)
               + 3*t*t*(-0.5 * pontos[i-1].x + 1.5 * pontos[i].x - 1.5 * pontos[i+1].x + 0.5 * pontos[i+2].x);
           y = 0.5*(-pontos[i-1].y + pontos[i+1].y)
               + 2*t*(pontos[i-1].y - 2.5*pontos[i].y + 2*pontos[i+1].y - 0.5*pontos[i+2].y)
               + 3*t*t*(-0.5*pontos[i-1].y + 1.5*pontos[i].y - 1.5*pontos[i+1].y + 0.5*pontos[i+2].y);
           z = 0.5*(-pontos[i-1].z + pontos[i+1].z)
               + 2*t*(pontos[i-1].z - 2.5*pontos[i].z + 2*pontos[i+1].z - 0.5*pontos[i+2].z)
               + 3*t*t*(-0.5*pontos[i-1].z + 1.5*pontos[i].z - 1.5*pontos[i+1].z + 0.5*pontos[i+2].z);
           pontos_curva1.push_back(Vetor3D(x,y,z));

           x = 2*(pontos[i-1].x - 2.5*pontos[i].x + 2*pontos[i+1].x - 0.5*pontos[i+2].x)
               + 6*t*(-0.5 * pontos[i-1].x + 1.5 * pontos[i].x - 1.5 * pontos[i+1].x + 0.5 * pontos[i+2].x);
           y = 2*(pontos[i-1].y - 2.5*pontos[i].y + 2*pontos[i+1].y - 0.5*pontos[i+2].y)
               + 6*t*(-0.5*pontos[i-1].y + 1.5*pontos[i].y - 1.5*pontos[i+1].y + 0.5*pontos[i+2].y);
           z = 2*(pontos[i-1].z - 2.5*pontos[i].z + 2*pontos[i+1].z - 0.5*pontos[i+2].z)
               + 6*t*(-0.5*pontos[i-1].z + 1.5*pontos[i].z - 1.5*pontos[i+1].z + 0.5*pontos[i+2].z);
           pontos_curva2.push_back(Vetor3D(x,y,z));
       }
   }
}

void inter()
{
    Vetor3D temp;
    float step = 0.01;
    for(int i = 0; i < 1; i++)
    {

        for(float t = 0; t <= 1; t += step)
        {

            temp.x =((-4.5*t*t*t +13.5*t*t -13.5*t +4.5)*pontos[i].x+
                     ( 9.0*t*t*t -22.5*t*t +18.0*t -4.5)*pontos[i+1].x+
                     (-5.5*t*t*t + 9.0*t*t - 4.5*t +1.0)*pontos[i+2].x+
                     ( 1.0*t*t*t + 0.0*t*t + 0.0*t +0.0)*pontos[i+3].x);

            temp.y =((-4.5*t*t*t +13.5*t*t -13.5*t +4.5)*pontos[i].y+
                     ( 9.0*t*t*t -22.5*t*t +18.0*t -4.5)*pontos[i+1].y+
                     (-5.5*t*t*t + 9.0*t*t - 4.5*t +1.0)*pontos[i+2].y+
                     ( 1.0*t*t*t + 0.0*t*t + 0.0*t +0.0)*pontos[i+3].y);

            temp.z =((-4.5*t*t*t +13.5*t*t -13.5*t +4.5)*pontos[i].z+
                     ( 9.0*t*t*t -22.5*t*t +18.0*t -4.5)*pontos[i+1].z+
                     (-5.5*t*t*t + 9.0*t*t - 4.5*t +1.0)*pontos[i+2].z+
                     ( 1.0*t*t*t + 0.0*t*t + 0.0*t +0.0)*pontos[i+3].z);

            pontos_inter.push_back(temp);
        }
    }
}

void hermite()
{

    // Fator de atenuação das tangentes T1 e T2
    GLdouble WG = 0.5;
    vector<Vetor3D> u(4);
    u[0] = pontos[1] - pontos[0];
    u[1] = pontos[2] - pontos[0];
    u[2] = pontos[3] - pontos[1];
    u[3] = pontos[3] - pontos[2];
    Vetor3D temp;
    float step = 0.01;
    for(int i = 0; i < 3; i++)
    {
        for(float t = 0; t <= 1; t += step)
        {


            temp.x =(( 2*t*t*t -3*t*t +0*t +1)*pontos[i].x+
                     (-2*t*t*t +3*t*t +0*t +0)*pontos[i+1].x+
                     ( 1*t*t*t -2*t*t +1*t +0)*WG*u[i].x+
                     ( 1*t*t*t -1*t*t +0*t +0)*WG*u[i+1].x);

            temp.y =(( 2*t*t*t -3*t*t +0*t +1)*pontos[i].y+
                     (-2*t*t*t +3*t*t +0*t +0)*pontos[i+1].y+
                     ( 1*t*t*t -2*t*t +1*t +0)*WG*u[i].y+
                     ( 1*t*t*t -1*t*t +0*t +0)*WG*u[i+1].y);

            temp.z =(( 2*t*t*t -3*t*t +0*t +1)*pontos[i].z+
                     (-2*t*t*t +3*t*t +0*t +0)*pontos[i+1].z+
                     ( 1*t*t*t -2*t*t +1*t +0)*WG*u[i].z+
                     ( 1*t*t*t -1*t*t +0*t +0)*WG*u[i+1].z);

            pontos_hermite.push_back(temp);
        }
    }
}

void bezier()
{
    Vetor3D temp;
    float step = 0.01;
    for(int i = 0; i < 1; i++)
    {
        for(float t = 0; t <= 1; t += step)
        {

            temp.x =((-1*t*t*t +3*t*t -3*t +1)*pontos[i].x+
                     ( 3*t*t*t -6*t*t +3*t +0)*pontos[i+1].x+
                     (-3*t*t*t +3*t*t +0*t +0)*pontos[i+2].x+
                     ( 1*t*t*t +0*t*t +0*t +0)*pontos[i+3].x);

            temp.y =((-1*t*t*t +3*t*t -3*t +1)*pontos[i].y+
                     ( 3*t*t*t -6*t*t +3*t +0)*pontos[i+1].y+
                     (-3*t*t*t +3*t*t +0*t +0)*pontos[i+2].y+
                     ( 1*t*t*t +0*t*t +0*t +0)*pontos[i+3].y);

            temp.z =((-1*t*t*t +3*t*t -3*t +1)*pontos[i].z+
                     ( 3*t*t*t -6*t*t +3*t +0)*pontos[i+1].z+
                     (-3*t*t*t +3*t*t +0*t +0)*pontos[i+2].z+
                     ( 1*t*t*t +0*t*t +0*t +0)*pontos[i+3].z);

            pontos_bezier.push_back(temp);
        }
    }
}

*/
void mult_matriz_vetor(float res[4], float matriz[16], float entr[4]) {
    for (int i = 0; i < 4; i++) {
        res[i] = 0.0;
        for (int j = 0; j < 4; j++) {
            //res[i] += matriz[4*i+j] * entr[j];
            res[i] += matriz[4*j+i] * entr[j]; //matriz^T.entr
        }
    }
}

void mostra_matriz_transform(float matriz[16], bool transposta = true) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (!transposta) {
                cout << setiosflags (ios::fixed) << setprecision(2) << matriz[4*i+j] << "  ";
            } else {
                cout << setiosflags (ios::fixed) << setprecision(2) << matriz[4*j+i] << "  "; //matriz^T
            }
        }
        cout << "\n";
    }
    //cout << "\n";
}

void imprime_coords_locais_globais()
{
    //imprimindo coords locais e coords globais
      //locais
        cout << "Coords locais: " << pl[0] << ", " << pl[1] << ", " << pl[2] << "\n";
      //globais
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
            glLoadIdentity();
              //composicao de transformacoes
              glTranslated(tx,ty,tz);
              glRotated(az,0,0,1);
              glRotated(ay,0,1,0);
              glRotated(ax,1,0,0);
            float mudanca_sist_coords[16];
            glGetFloatv(GL_MODELVIEW_MATRIX,mudanca_sist_coords);
            cout << "Matriz mudanca sist coords local=>global (T.Rz.Ry.Rx):\n";
            mostra_matriz_transform(mudanca_sist_coords);
            mult_matriz_vetor(pg,mudanca_sist_coords,pl);
            cout << "Coords globais: " << pg[0] << ", " << pg[1] << ", " << pg[2] << "\n\n";
        glPopMatrix();
}

void resize(int w, int h)
{
    width = w;
    height = h;
}



void atualizarCam ()
{
    vector<Vetor3D>& pontos = curva.getPontos();
    vector<Vetor3D>& derivada1 = curva.getDerivada1();
    vector<Vetor3D>& derivada2 = curva.getDerivada2();

    Vetor3D ctoe;
    switch (view) {
    case 0:
        gluLookAt(0.0, 1.0, 10.0
                  , 0.0, 1.0, 0.0
                  , 0.0, 1.0, 0.0);

        cam->e = pontos[t];
        cam->c = {0.0, 1.0, 0.0};
        cam->u = {0.0, 1.0, 0.0};
        break;
    case 1:
        cam->e = pontos[t];
        cam->c = pontos[t] + derivada1[t];
        cam->u = derivada2[t];
        cam->u.normaliza();

        ctoe = cam->e - cam->c;
        ctoe.normaliza();

        cam->e = cam->e + cam->u*0.3;
        cam->e = cam->e + ctoe*0.7;
        break;
    case 2:
        cam->e = {10.0, 2.0, 10.0};
        cam->c = pontos[t];
        cam->u = {0.0, 1.0, 0.0};
        break;
    default:
        break;
    }
}

void desenharCurva()
{
    glBegin(GL_LINE_STRIP);
      glColor3d(0,0,1);
      for (int i = 0; i < curva.getNumPontos(); i++)
          glVertex3d(curva[i].x, curva[i].y, curva[i].z);
    glEnd();
}

void displayInner()
{
    vector<Vetor3D>& pontos = curva.getPontos();

    // Calculando matriz de transformação dos objetos
    int t1 = (t > 1 ? t-2 : curva.getNumPontos()+(t-2));
    int t2 = (t+2) % curva.getNumPontos();
    GLdouble *m  = curva.getMatTrans(t );
    GLdouble *m1 = curva.getMatTrans(t1);
    GLdouble *m2 = curva.getMatTrans(t2);

    // Sistema Global
    glPushMatrix();
      glColor3d(0.0,0.0,0.0);
      Desenha::drawGrid( 10, 0, 10, 1 );
    glPopMatrix();

    // Pontos de referência das laterais
    glPushMatrix();
      glColor3d(1,0,0);
      glTranslated(-10,2.5,0);
      glScaled(1,5,1);
      glutSolidCube(1);
    glPopMatrix();
    glPushMatrix();
      glColor3d(0,1,0);
      glTranslated(0,2.5,-10);
      glScaled(1,5,1);
      glutSolidCube(1);
    glPopMatrix();
    glPushMatrix();
      glColor3d(0,0,1);
      glTranslated(10,2.5,0);
      glScaled(1,5,1);
      glutSolidCube(1);
    glPopMatrix();
    glPushMatrix();
      glColor3d(0,0,0);
      glTranslated(0,2.5,10);
      glScaled(1,5,1);
      glutSolidCube(1);
    glPopMatrix();

    // Carrinho
    glPushMatrix();
      glColor3d(0.9,0.9,0.9);
      glMultTransposeMatrixd(m);
      glutSolidTorus(0.04, 0.08, 6, 50);
    glPopMatrix();
    glPushMatrix();
      glColor3d(0.9,0.9,0.9);
      glMultTransposeMatrixd(m1);
      glutSolidTorus(0.04, 0.08, 6, 50);
    glPopMatrix();
    glPushMatrix();
      glColor3d(0.9,0.9,0.9);
      glMultTransposeMatrixd(m2);
      glutSolidTorus(0.04, 0.08, 6, 50);
    glPopMatrix();

    for (int i = 0; i < int(pontos.size()); i++)
    {
        GLdouble *mi = curva.getMatTrans(i);
        glPushMatrix();
          glMultTransposeMatrixd(m);
          glutSolidCube(0.05);
        glPopMatrix();
        delete mi;
    }


    if (showPoints)
    {
        for (auto p : pontosControle)
        {
            glPushMatrix();
              glColor4d(1,0,0,0.1);
              glTranslated(p.x, p.y, p.z);
              glutSolidCube(0.1);
            glPopMatrix();
        }
    }
    desenharCurva();

    delete m;
    delete m1;
    delete m2;
}

void displayInit()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const float ar = height>0 ? (float) width / (float) height : 1.0;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(30.,ar,0.1,1000.);

    glMatrixMode(GL_MODELVIEW);

    glViewport(0, 0, width, height);
      glLoadIdentity();
        atualizarCam();
        gluLookAt(cam->e.x,cam->e.y,cam->e.z
                  , cam->c.x,cam->c.y,cam->c.z
                  , cam->u.x,cam->u.y,cam->u.z);
    displayInner();


}

void displayEnd()
{
    if (play)
    {
        t++;
        t = t % int(curva.getNumPontos());
    }
    glutSwapBuffers();
}

void display()
{
    displayInit();
    displayEnd();
}

void mouseButton(int button, int state, int x, int y) {
    // if the left button is pressed
    if (button == GLUT_LEFT_BUTTON) {
        // when the button is pressed
        if (state == GLUT_DOWN) {
            lbpressed = true;
        } else {// state = GLUT_UP
            lbpressed = false;
        }
    }
    // if the middle button is pressed
    if (button == GLUT_MIDDLE_BUTTON) {
        // when the button is pressed
        if (state == GLUT_DOWN) {
            mbpressed = true;
        } else {// state = GLUT_UP
            mbpressed = false;
        }
    }
    // if the left button is pressed
    if (button == GLUT_RIGHT_BUTTON) {
        // when the button is pressed
        if (state == GLUT_DOWN) {
            rbpressed = true;
        } else {// state = GLUT_UP
            rbpressed = false;
        }
    }

    last_x = x;
    last_y = y;
}

void mouseMove(int x, int y) {
    float fator = 10.0;
    if (lbpressed && !rbpressed && !mbpressed) {
        if (!trans_obj) {
            if (!manual_cam) {
                if (!change_manual_cam) {
                    cam->rotatex(y,last_y);
                    cam->rotatey(x,last_x);
                } else {
                    cam2->rotatex(last_y,y);
                    cam2->rotatey(last_x,x);
                }
            } else {
                if (!change_manual_cam) {
                    cam->rotatex(last_y,y);
                    cam->rotatey(last_x,x);
                } else {
                    cam2->rotatex(y,last_y);
                    cam2->rotatey(x,last_x);
                }
            }
        } else {
            ax += (y - last_y)/fator;
            ay += (x - last_x)/fator;
        }
    }
    fator = 100.0;
    if (!lbpressed && rbpressed && !mbpressed) {
        if (!trans_obj) {
            if (!manual_cam) {
                if (!change_manual_cam) {
                    cam->translatex(x,last_x);
                    cam->translatey(y,last_y);
                } else {
                    cam2->translatex(last_x,x);
                    cam2->translatey(last_y,y);
                }
            } else {
                if (!change_manual_cam) {
                    cam->translatex(last_x,x);
                    cam->translatey(last_y,y);
                } else {
                    cam2->translatex(x,last_x);
                    cam2->translatey(y,last_y);
                }
            }
        } else {
            tx += (x - last_x)/fator;
            ty += -(y - last_y)/fator;
        }
    }
    if (lbpressed && rbpressed && !mbpressed) {
        if (!trans_obj) {
            if (!manual_cam) {
                if (!change_manual_cam) {
                    cam->zoom(y,last_y);
                } else {
                    cam2->zoom(last_y,y);
                }
            } else {
                if (!change_manual_cam) {
                    cam->zoom(last_y,y);
                } else {
                    cam2->zoom(y,last_y);
                }
            }
        } else {
            tz += (y - last_y)/fator;
            fator = 10.0;
            az += -(x - last_x)/fator;
        }
    }
    fator = 100.0;
    if (!lbpressed && !rbpressed && mbpressed) {
        if (!trans_obj) {
        } else {
            sx += (x - last_x)/fator;
            sy += -(y - last_y)/fator;
        }
    }
    if (lbpressed && !rbpressed && mbpressed) {
        if (!trans_obj) {
        } else {
            sz += (y - last_y)/fator;
        }
    }
    if (!lbpressed && rbpressed && mbpressed) {
        if (!trans_obj) {
        } else {
            pl[0] += (x - last_x)/fator;
            pl[1] += -(y - last_y)/fator;
            imprime_coords_locais_globais();
        }
    }

    last_x = x;
    last_y = y;

}

void key(unsigned char key, int x, int y)
{

    switch (key)
    {
        // Controle de execução
        case ' ':
            play = !play;
            break;
        case '>':
            if (!play)
                t++;
                t = t % curva.getNumPontos();
            break;
        case '<':
            if (!play)
            {
                if (t > 0)
                    t--;
                else
                    t = curva.getNumPontos()-1;
            }
            break;
        // Mudar curva
        case 'c':
            curvaSelec = TipoCurva((int(curvaSelec)+1)%5);
            break;

        case 'q':
            exit(0);
            break;
        case 'F':
            glutFullScreen();
            break;
        case 'f':
            glutReshapeWindow(800,600);
            break;

        case '+':
            slices++;
            stacks++;
            break;

        case '-':
            if (slices>3 && stacks>3)
            {
                slices--;
                stacks--;
            }
            break;

        case 'X':
            //ax+=delta;
            if(trans_obj) ax+=delta;
            else { pl[0] += 0.01; imprime_coords_locais_globais(); };
            break;

        case 'Y':
            //ay+=delta;
            if(trans_obj) ay+=delta;
            else { pl[1] += 0.01; imprime_coords_locais_globais(); };
            break;

        case 'Z':
            //az+=delta;
            if(trans_obj) az+=delta;
            else { pl[2] += 0.01; imprime_coords_locais_globais(); };
            break;

        case 'x':
            //ax-=delta;
            if(trans_obj) ax-=delta;
            else { pl[0] -= 0.01; imprime_coords_locais_globais(); };
            break;

        case 'y':
            //ay-=delta;
            if(trans_obj) ay-=delta;
            else { pl[1] -= 0.01; imprime_coords_locais_globais(); };
            break;

        case 'z':
            //az-=delta;
            if(trans_obj) az-=delta;
            else { pl[2] -= 0.01; imprime_coords_locais_globais(); };
            break;

        case 'i':
            ax=ay=az=0.0;
            tx=ty=tz=0.0;
            sx=sy=sz=1.0;
            break;

        case 't':
            trans_obj = !trans_obj;
            break;
        case 'C':
            break;
        case 'v':
            view++;
            view =  view%numViews;
            break;
        case 'p':
            showPoints = !showPoints;
        break;
        case 's':
            //save current camera location
            savedCamera[0] = cam->e.x;
            savedCamera[1] = cam->e.y;
            savedCamera[2] = cam->e.z;
            savedCamera[3] = cam->c.x;
            savedCamera[4] = cam->c.y;
            savedCamera[5] = cam->c.z;
            savedCamera[6] = cam->u.x;
            savedCamera[7] = cam->u.y;
            savedCamera[8] = cam->u.z;
            break;

        case 'm':
            if (!manual_cam) {
                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                    glLoadIdentity();
                      glTranslated(tx,ty,tz);
                      glRotated(az,0,0,1);
                      glRotated(ay,0,1,0);
                      glRotated(ax,1,0,0);
                      glScaled(sx,sy,sz);
                    float transform[16];
                    glGetFloatv(GL_MODELVIEW_MATRIX,transform);
                    cout << "Matriz composicao de transformacoes (T.Rz.Ry.Rx.S):\n";
                    mostra_matriz_transform(transform);
                    cout << "\n";
                glPopMatrix();
            } else {
                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                    glLoadIdentity();
                      gluLookAt(cam2->e.x,cam2->e.y,cam2->e.z, cam2->c.x,cam2->c.y,cam2->c.z, cam2->u.x,cam2->u.y,cam2->u.z);
                    float transform[16];
                    glGetFloatv(GL_MODELVIEW_MATRIX,transform);
                    cout << "Matriz gluLookAt:\n";
                    mostra_matriz_transform(transform);
                    cout << "\n";
                glPopMatrix();
            }
            break;
    }

    glutPostRedisplay();
}

void idle(void)
{
    glutPostRedisplay();
}

/* Program entry point */

int main(int argc, char *argv[])
{
    // Gerando curva

    curva.calcular(pontosControle);

    //chamadas de inicializacao da GLUT
        glutInit(&argc, argv);
        glutInitWindowSize(width,height);
        glutInitWindowPosition(10,10);
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

        glutCreateWindow("Câmera");

        glutReshapeFunc(resize);
        glutDisplayFunc(display);
        glutKeyboardFunc(key);
        glutIdleFunc(idle);

        glutMouseFunc(mouseButton);
        glutMotionFunc(mouseMove);

    //chamadas de inicializacao da OpenGL
        glClearColor(0.9,0.9,0.9,1);
        //glClearColor(0.3,0.3,0.3,1.0);

        glEnable(GL_LIGHTING);
        glEnable(GL_COLOR_MATERIAL);
        //glEnable(GL_CULL_FACE);
        //glCullFace(GL_BACK);
        glEnable(GL_NORMALIZE); //mantem a qualidade da iluminacao mesmo quando glScalef eh usada

        glShadeModel(GL_SMOOTH);
        //glShadeModel(GL_FLAT);

        glEnable(GL_DEPTH_TEST);
        //glDepthFunc(GL_LESS);

        //glEnable(GL_BLEND); //habilita a transparencia
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //definindo uma luz
            glEnable(GL_LIGHT0);

            const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
            const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
            const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
            const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

            const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
            const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
            const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
            const GLfloat high_shininess[] = { 100.0f };

            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
            glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
            glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
            glLightfv(GL_LIGHT0, GL_POSITION, light_position);

            glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
            glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
            glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
            glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    //iniciando o loop principal da glut
        glutMainLoop();

    return EXIT_SUCCESS;
}
