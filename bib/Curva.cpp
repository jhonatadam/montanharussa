#include "Curva.h"

const double Curva::matrizes[5][16] =
{
    { // CATMULL
       0.0,  1.0,  0.0,  0.0,
      -0.5,  0.0,  0.5,  0.0,
       1.0, -2.5,  2.0, -0.5,
      -0.5,  1.5, -1.5,  0.5
    },
    { // HERMITE
      -1/6,  3/6, -3/6, 1/6,
       3/6, -6/6,  0/6, 4/6,
      -3/6,  3/6,  3/6, 1/6,
       1/6,  0/6,  0/6, 0/6
    },
    { // BEZIER
      -1/6,  3/6, -3/6, 1/6,
       3/6, -6/6,  0/6, 4/6,
      -3/6,  3/6,  3/6, 1/6,
       1/6,  0/6,  0/6, 0/6
    },
    { // B_SPLINE
       -0.1667,  0.5, -0.5, 0.1667,
        0.5000, -1.0,  0.0, 0.6667,
       -0.5000,  0.5,  0.5, 0.1667,
        0.1667,  0.0,  0.0, 0.0000
    },
    { // INTER
       -1/6,  3/6, -3/6, 1/6,
        3/6, -6/6,  0/6, 4/6,
       -3/6,  3/6,  3/6, 1/6,
        1/6,  0/6,  0/6, 0/6
    }
};


Curva::Curva(TipoCurva tipo)
{
    this->tipo = tipo;
}

Curva::~Curva()
{

}

void Curva::calcular(const std::vector<Vetor3D> &pontosControle)
{
    Vetor3D temp;
    double c[4];

    for(int i = 0; i < int(pontosControle.size())-3; i++)
    {
        for(float t = 0; t <= 1; t += PASSO)
        {

            c[0] = this->matrizes[tipo][0]*t*t*t +
                   this->matrizes[tipo][1]*t*t   +
                   this->matrizes[tipo][2]*t     +
                   this->matrizes[tipo][3]       ;

            c[1] = this->matrizes[tipo][4]*t*t*t +
                   this->matrizes[tipo][5]*t*t   +
                   this->matrizes[tipo][6]*t     +
                   this->matrizes[tipo][7]       ;

            c[2] = this->matrizes[tipo][ 8]*t*t*t +
                   this->matrizes[tipo][ 9]*t*t   +
                   this->matrizes[tipo][10]*t     +
                   this->matrizes[tipo][11]       ;

            c[3] = this->matrizes[tipo][12]*t*t*t +
                   this->matrizes[tipo][13]*t*t   +
                   this->matrizes[tipo][14]*t     +
                   this->matrizes[tipo][15]       ;

            temp.x = c[0] * pontosControle[  i].x +
                     c[1] * pontosControle[i+1].x +
                     c[2] * pontosControle[i+2].x +
                     c[3] * pontosControle[i+3].x ;

            temp.y = c[0] * pontosControle[  i].y +
                     c[1] * pontosControle[i+1].y +
                     c[2] * pontosControle[i+2].y +
                     c[3] * pontosControle[i+3].y ;

            temp.z = c[0] * pontosControle[  i].z +
                     c[1] * pontosControle[i+1].z +
                     c[2] * pontosControle[i+2].z +
                     c[3] * pontosControle[i+3].z ;

            this->pontos.push_back(temp);

            c[0] = this->matrizes[tipo][0]*3*t*t +
                   this->matrizes[tipo][1]*2*t   +
                   this->matrizes[tipo][2]       ;

            c[1] = this->matrizes[tipo][4]*3*t*t +
                   this->matrizes[tipo][5]*2*t   +
                   this->matrizes[tipo][6]       ;

            c[2] = this->matrizes[tipo][ 8]*3*t*t +
                   this->matrizes[tipo][ 9]*2*t   +
                   this->matrizes[tipo][10]       ;

            c[3] = this->matrizes[tipo][12]*3*t*t +
                   this->matrizes[tipo][13]*2*t   +
                   this->matrizes[tipo][14]       ;

            temp.x = c[0] * pontosControle[  i].x +
                     c[1] * pontosControle[i+1].x +
                     c[2] * pontosControle[i+2].x +
                     c[3] * pontosControle[i+3].x ;

            temp.y = c[0] * pontosControle[  i].y +
                     c[1] * pontosControle[i+1].y +
                     c[2] * pontosControle[i+2].y +
                     c[3] * pontosControle[i+3].y ;

            temp.z = c[0] * pontosControle[  i].z +
                     c[1] * pontosControle[i+1].z +
                     c[2] * pontosControle[i+2].z +
                     c[3] * pontosControle[i+3].z ;

            this->derivada1.push_back(temp);

            c[0] = this->matrizes[tipo][0]*6*t +
                   this->matrizes[tipo][1]*2   ;

            c[1] = this->matrizes[tipo][4]*6*t +
                   this->matrizes[tipo][5]*2   ;

            c[2] = this->matrizes[tipo][ 8]*6*t +
                   this->matrizes[tipo][ 9]*2   ;

            c[3] = this->matrizes[tipo][12]*6*t +
                   this->matrizes[tipo][13]*2   ;

            temp.x = c[0] * pontosControle[  i].x +
                     c[1] * pontosControle[i+1].x +
                     c[2] * pontosControle[i+2].x +
                     c[3] * pontosControle[i+3].x ;

            temp.y = c[0] * pontosControle[  i].y +
                     c[1] * pontosControle[i+1].y +
                     c[2] * pontosControle[i+2].y +
                     c[3] * pontosControle[i+3].y ;

            temp.z = c[0] * pontosControle[  i].z +
                     c[1] * pontosControle[i+1].z +
                     c[2] * pontosControle[i+2].z +
                     c[3] * pontosControle[i+3].z ;

            this->derivada2.push_back(temp);
        }
    }
}

std::vector<Vetor3D>& Curva::getPontos()
{
    return pontos;
}

std::vector<Vetor3D> &Curva::getDerivada1()
{
    return derivada1;
}

std::vector<Vetor3D> &Curva::getDerivada2()
{
    return derivada2;
}

double* Curva::getMatTrans(int t)
{
    double* m = new double[16];
    Vetor3D _z = derivada1[t]*-1;
    Vetor3D _y = derivada2[t];

    Vetor3D _x = _y.prodVetorial(_z);
    _y = _z.prodVetorial(_x);
    _z.normaliza();
    _y.normaliza();
    _x.normaliza();

    m[ 0] = _x.x; m[ 1] = _y.x; m[ 2] = _z.x; m[ 3] = pontos[t].x;
    m[ 4] = _x.y; m[ 5] = _y.y; m[ 6] = _z.y; m[ 7] = pontos[t].y;
    m[ 8] = _x.z; m[ 9] = _y.z; m[10] = _z.z; m[11] = pontos[t].z;
    m[12] =    0; m[13] =    0; m[14] =    0; m[15] =           1;

    return m;
}

Vetor3D &Curva::operator[](int i)
{
    return pontos[i];
}

int Curva::getNumPontos()
{
    return int(pontos.size());
}
