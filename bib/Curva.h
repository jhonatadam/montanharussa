#ifndef CURVA_H
#define CURVA_H

#include <vector>
#include <iostream>

#include "Vetor3D.h"

#define PASSO 0.01

enum TipoCurva {CATMULL, HERMITE, BEZIER, B_SPLINE, INTER};

class Curva
{

    TipoCurva tipo;
    static const double matrizes[5][16];
    std::vector<Vetor3D> pontos;
    std::vector<Vetor3D> derivada1;
    std::vector<Vetor3D> derivada2;

public:
    Curva(TipoCurva tipo);
    ~Curva();

    void calcular(const std::vector<Vetor3D> &pontosControle);
    std::vector<Vetor3D>& getPontos();
    std::vector<Vetor3D>& getDerivada1();
    std::vector<Vetor3D>& getDerivada2();
    double * getMatTrans(int t);
    Vetor3D& operator[](int i);
    int getNumPontos();
};

#endif // CURVA_H
